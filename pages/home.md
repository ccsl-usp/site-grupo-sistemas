---
layout: home
title: Software Systems Research Group
description: at the Department of Computer Science of the Institute of Mathematics and Statistics (IME) at the University of São Paulo (USP)
background: /assets/img/studentsss2.jpeg
permalink: /

---
## About us
Our group focuses on high-quality scientific and technological research on a broad spectrum of topics related to Software, including Software Engineering, Data Engineering, High-Performance Computing, Networking and Distributed Systems, and Data Science.

We strive to contribute to Science and Society by developing innovative research and open artifacts like Free and Open Source Software (FOSS). Our research results are applied not only to computer science but also to various areas of application, including smart cities, urban mobility, health, and education. As a result of this work, we have published papers in various high-profile international journals and conferences.

As part of our mission, we focus on innovative education at undergrad, graduate, and continuing education levels. Our group has advised over 100 Masters, 30 Doctoral students, and 15 post-docs. We also worked with over 200 undergraduate research students.

At our core, we believe that collaboration is the key to unlocking innovation. That's why we're always looking to team up with other universities, research institutions, governmental bodies, startups, and companies.Besides dozens of collaborations within Brazil, we have worked closely with researchers from MIT, UC Berkeley, Georgia Tech, Grenoble, Lancaster, North Arizona University, University of Alberta, Umass Amherst, ETH Zurich, Eindhoven University of Technology, Universidad Politécnica de Madrid, INRIA, Paris VI, University of Illinois at Urbana-Champaign, IBM, HPE Labs, etc.


