---
layout: home
title: Infrastructure
description: 
background: /assets/img/infrastructurex.jpg
permalink: /infrastructure/

---
## CCSL - Free Software Competence Center 
USP's FLOSS Competence Center (CCSL) brings together activities related to undergraduate and graduate teaching, research, development and dissemination of software at USP and has centers formally established in the city of São Paulo at IME and in São Carlos at ICMC. Its main objective is to encourage the development, research and use of free software inside and outside the university.

<figure style="display:block; width:70%; margin-left:auto; margin-right:auto">
<img src="{{ "/assets/img/CCSLx.JPEG"|relative_url }}" style="width:100%">
<figcaption>CCSL - Free Software Competence Center</figcaption>
</figure>


## Laboratories
Our 22-person facility offers an environment conducive to collaboration and experimentation, where each team member has access to the tools and resources needed to turn ideas into reality. This is where our research group works to develop innovative solutions in the field of software.

<div style="display:flex; width:95%; justify-content:space-between">

<figure style="display:block; width:48%">
<img src="{{ "/assets/img/syslabx.jpg"|relative_url }}" style="width:100%">
<figcaption>Software Systems Laboratory - 120</figcaption>
</figure>

<figure style="display:block; width:48%">
<img src="{{ "/assets/img/lab16x.jpg"|relative_url }}" style="width:100%">
<figcaption>X laboratory - 16</figcaption>
</figure>

</div>

## Imre Simon auditorium
With the capacity to comfortably seat 50, our auditorium is designed to provide an exceptional experience for all participants.
Equipped with good technology, our auditorium offers high-quality audiovisual resources, ensuring that presentations, lectures and events are conducted efficiently and functionally.

<figure style="display:block; width:70%; margin-left:auto; margin-right:auto">
<img src="{{ "/assets/img/auditoriumx.JPG"|relative_url }}" style="width:100%">
<figcaption>Imre Simon Auditorium</figcaption>
</figure>

## Lobby
With its modern architecture and contemporary design, our lobby is equipped with comfortable seating, soft lighting and a tranquil atmosphere, making it more than just a space to pass through - it's a place to relax, socialize and be inspired. Whether you're waiting for a meeting, meeting friends or simply enjoying a quiet moment, this is the ideal place.

<figure style="display:block; width:70%; margin-left:auto; margin-right:auto">
<img src="{{ "/assets/img/imagex.jpeg"|relative_url }}" style="width:100%">
<figcaption>CCSL Lobby</figcaption>
</figure>
