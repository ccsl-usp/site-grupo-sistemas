---
layout: home
title: Projects
description: 
background: /assets/img/projectsx.jpeg
permalink: /projects/

--- 
## InterSCity
The Future Internet will integrate large-scale systems constructed from the composition of thousands of distributed services, while interacting directly with the physical world via sensors and actuators, which compose the Internet of Things.

InterSCity researchers seek to apply novel Computer Science and Technology techniques to tackle urban social problems in underprivileged neighborhoods and low-income populations, leveraging existing data and collecting and analyzing new datasets to support Evidence-based Public Policymaking. More information can be found at <https://interscity.org/>


## Starling
A FAPESP regular research project coordinated by Prof. Daniel Batista. The project investigates security and resource allocation on Beyond 5G technologies via artificial intelligence techniques. More information can be found at <https://bv.fapesp.br/en/auxilios/113042/starling-security-and-resource-allocation-on-b5g-via-artificial-intelligence-techniques/>

## Trends in high-performance computing, from resource management to new computer architectures
This is a FAPESP-supported Thematic Project led by Prof. Dr. Alfredo Goldman vel Lejbman, professor at the Institute of Mathematics and Statistics (IME), University of São Paulo (USP), São Paulo, SP, Brazil.

The main objective of the project is to bring together the efforts and knowledge of several researchers in the field of High Performance Computing (HPC) in order to produce high-impact research in two areas: resource management and new computer architectures. More information can be found at <https://hpc.ime.usp.br/>

## Development of infrastructure for integration and improvement of accessibility of socio-environmental data and support to public policies and Semantic integration and intelligent query interfaces for socioeconomic data to support public policies 
These two projects are coordinated by Prof. Kelly Rosa Braghetto and both aim to expand access and accessibility to strategic data for planning community actions and supporting public policy-making in areas such as Education, Culture, and Public Health by developing free software tools to integrate heterogeneous data on a large scale and with advanced resources for describing and querying data. In particular, the project will develop mechanisms for metadata management and semantic enrichment of databases to facilitate complex queries and knowledge discovery. It will also develop innovative tools to simplify queries on spatial and temporal data for non-technical users, such as database query interfaces based on natural language. 
Other information can be found at: <https://bv.fapesp.br/en/auxilios/113028/development-of-infrastructure-for-integration-and-improvement-of-accessibility-of-socio-environmenta/> 





